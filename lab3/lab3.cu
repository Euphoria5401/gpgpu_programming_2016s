#include "lab3.h"
#include <cstdio>

#define ITERATECOUNT 20000
/*
I implement poisson image editing in lab3. 
Some of variable name please refer to original paper and lab3.pdf
*/
__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

__global__ void SimpleClone(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox
)
{
	const int yt = blockIdx.y * blockDim.y + threadIdx.y;
	const int xt = blockIdx.x * blockDim.x + threadIdx.x;
	const int curt = wt*yt+xt;
	if (yt < ht and xt < wt and mask[curt] > 127.0f) {
		const int yb = oy+yt, xb = ox+xt;
		const int curb = wb*yb+xb;
		if (0 <= yb and yb < hb and 0 <= xb and xb < wb) {
			output[curb*3+0] = target[curt*3+0];
			output[curb*3+1] = target[curt*3+1];
			output[curb*3+2] = target[curt*3+2];
		}
	}
}

__global__ void CalculateFixed(const float *background,
							   const float *target,
							   const float *mask,
							   float *fixed,
							   const int wb, const int hb, const int wt, const int ht,
							   const int oy, const int ox) {
	int yt = blockIdx.y * blockDim.y + threadIdx.y;
	int xt = blockIdx.x * blockDim.x + threadIdx.x;
	int curt = wt*yt+xt;
	if (yt >= ht || xt >= wt) return;
	if (mask[curt] < 127.0f) return;
	int nt = curt - wt;
	int st = curt + wt;
	int westt = curt - 1;
	int et = curt + 1;
	int mask_nt = yt > 0 ? mask[nt] : 0;
	int mask_st = yt < ht - 1 ? mask[st] : 0;
	int mask_wt = xt > 0 ? mask[westt] : 0;
	int mask_et = xt < wt - 1 ? mask[et] : 0;
	float temp_fixed_r = 0;
	float temp_fixed_g = 0;
	float temp_fixed_b = 0;
	int yb = oy+yt, xb = ox+xt;
	if (yb < 0 || yb >= hb || xb < 0 || xb >= wb) {
		return;
	}
	int curb = wb*yb+xb;
	int nb = curb - wb;
	int sb = curb + wb;
	int westb = curb - 1;
	int eb = curb + 1;
	//calculate Vpq
	if (yt > 0 && yb > 0) {
		temp_fixed_r += target[3*curt + 0] - target[3*nt + 0];
		temp_fixed_g += target[3*curt + 1] - target[3*nt + 1];
		temp_fixed_b += target[3*curt + 2] - target[3*nt + 2];
	}
	if (yt < ht - 1 && yb < hb - 1) {
		temp_fixed_r += target[3*curt + 0] - target[3*st + 0];
		temp_fixed_g += target[3*curt + 1] - target[3*st + 1];
		temp_fixed_b += target[3*curt + 2] - target[3*st + 2];
	}
	if (xt > 0 && xb > 0) {
		temp_fixed_r += target[3*curt + 0] - target[3*westt + 0];
		temp_fixed_g += target[3*curt + 1] - target[3*westt + 1];
		temp_fixed_b += target[3*curt + 2] - target[3*westt + 2];
	}
	if (xt < wt - 1 && xb < wb - 1) {
		temp_fixed_r += target[3*curt + 0] - target[3*et + 0];
		temp_fixed_g += target[3*curt + 1] - target[3*et + 1];
		temp_fixed_b += target[3*curt + 2] - target[3*et + 2];
	}
	//calculat fq*
	if (mask_nt < 127.0f && yb > 0) {
		temp_fixed_r += background[3*nb + 0];
		temp_fixed_g += background[3*nb + 1];
		temp_fixed_b += background[3*nb + 2];
	}
	if (mask_st < 127.0f && yb < hb - 1) {
		temp_fixed_r += background[3*sb + 0];
		temp_fixed_g += background[3*sb + 1];
		temp_fixed_b += background[3*sb + 2];
	}
	if (mask_wt < 127.0f && xb > 0) {
		temp_fixed_r += background[3*westb + 0];
		temp_fixed_g += background[3*westb + 1];
		temp_fixed_b += background[3*westb + 2];
	}
	if (mask_et < 127.0f && xb < wb - 1) {
		temp_fixed_r += background[3*eb + 0];
		temp_fixed_g += background[3*eb + 1];
		temp_fixed_b += background[3*eb + 2];
	}
	fixed[3*curt + 0] = temp_fixed_r;
	fixed[3*curt + 1] = temp_fixed_g;
	fixed[3*curt + 2] = temp_fixed_b;
}

__global__ void PoissonImageCloningIteration(const float *fixed,
											 const float *mask, 
											 float *src, float *dst, 
											 const int wt, const int ht,
											 const int wb, const int hb,
											 const int oy, const int ox) {
	int yt = blockIdx.y * blockDim.y + threadIdx.y;
	int xt = blockIdx.x * blockDim.x + threadIdx.x;
	int curt = wt*yt+xt;
	if (yt >= ht || xt >= wt) return;
	if (mask[curt] < 127.0f) return;
	int nt = curt - wt;
	int st = curt + wt;
	int westt = curt - 1;
	int et = curt + 1;
	int mask_nt = yt > 0 ? mask[nt] : 0;
	int mask_st = yt < ht - 1 ? mask[st] : 0;
	int mask_wt = xt > 0 ? mask[westt] : 0;
	int mask_et = xt < wt - 1 ? mask[et] : 0;
	int yb = oy+yt, xb = ox+xt;
	if (yb < 0 || yb >= hb || xb < 0 || xb >= wb) {
		return;
	}
	int np = 0;//neighbor_point_count
	float fq_r = 0;
	float fq_g = 0;
	float fq_b = 0;
	//calculate np
	if (yb > 0) np++;
	if (yb < hb - 1) np ++;
	if (xb > 0) np ++;
	if (xb < wb - 1) np ++;
	//calculate fq
	if (yb != 0) {
		if (mask_nt > 127.0f) {
			fq_r += src[3*nt + 0];
			fq_g += src[3*nt + 1];
			fq_b += src[3*nt + 2];
		}
	}
	if (yb != hb - 1) {
		if (mask_st > 127.0f) {
			fq_r += src[3*st + 0];
			fq_g += src[3*st + 1];
			fq_b += src[3*st + 2];
		}
	}
	if (xb != 0) {
		if (mask_wt > 127.0f) {
			fq_r += src[3*westt + 0];
			fq_g += src[3*westt + 1];
			fq_b += src[3*westt + 2];
		}
	}
	if (xb != wb - 1) {
		if (mask_et > 127.0f) {
			fq_r += src[3*et + 0];
			fq_g += src[3*et + 1];
			fq_b += src[3*et + 2];
		}
	}
	__syncthreads();
	dst[3*curt + 0] = (fixed[3*curt + 0] + fq_r) / np;
	dst[3*curt + 1] = (fixed[3*curt + 1] + fq_g) / np;
	dst[3*curt + 2] = (fixed[3*curt + 2] + fq_b) / np;
}

void PoissonImageCloning(
	const float *background,
	const float *target,
	const float *mask,
	float *output,
	const int wb, const int hb, const int wt, const int ht,
	const int oy, const int ox)
{
	/*cudaMemcpy(output, background, wb*hb*sizeof(float)*3, cudaMemcpyDeviceToDevice);
	SimpleClone<<<dim3(CeilDiv(wt,32), CeilDiv(ht,16)), dim3(32,16)>>>(
		background, target, mask, output,
		wb, hb, wt, ht, oy, ox
	);*/
	float *fixed, *buffer1, *buffer2;
	cudaMalloc((void**) &fixed, 3*wt*ht*sizeof(float));
	cudaMalloc((void**) &buffer1, 3*wt*ht*sizeof(float));
	cudaMalloc((void**) &buffer2, 3*wt*ht*sizeof(float));

	dim3 gdim(CeilDiv(wt, 32), CeilDiv(ht, 16)), bdim(32, 16);
	CalculateFixed<<<gdim, bdim>>>(
		background, target, mask, fixed,
		wb, hb, wt, ht, oy, ox);
	cudaMemcpy(buffer1, target, 3*wt*ht*sizeof(float), cudaMemcpyDeviceToDevice);

	for (int i =0; i < ITERATECOUNT; i++) {
		PoissonImageCloningIteration<<<gdim, bdim>>>(
			fixed, mask, buffer1, buffer2, wt, ht, wb, hb, oy, ox);
		//cudaThreadSynchronize();
		PoissonImageCloningIteration<<<gdim, bdim>>>(
			fixed, mask, buffer2, buffer1, wt, ht, wb, hb, oy, ox);
	}

	cudaMemcpy(output, background, wb*hb*sizeof(float)*3, cudaMemcpyDeviceToDevice);
	SimpleClone<<<gdim, bdim>>>(
		background, buffer1, mask, output,
		wb, hb, wt, ht, oy, ox);

	cudaFree(fixed);
	cudaFree(buffer1);
	cudaFree(buffer2);
}
