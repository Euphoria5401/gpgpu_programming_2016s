#include "lab1.h"
#include <algorithm>
#include <iostream>
static const unsigned W = 640;//column number640
static const unsigned H = 480;//row number480
static const unsigned NFRAME = 240;
static const int SIZE = (W+2)*(H+2);
static const float DT = 1.0;
static const float VISCOSITY = 0.0;
static const float DIFFUSE = 0.0;
static const int LINEARSOLVERTIMES = 10;
static float* vector_field_x = (float*) malloc(SIZE*sizeof(float));
static float* vector_field_y = (float*) malloc(SIZE*sizeof(float));
static float* scalar_field_1 = (float*) malloc(SIZE*sizeof(float));
static float* scalar_field_2 = (float*) malloc(SIZE*sizeof(float));
static float* vector_field_x_prev = (float*) malloc(SIZE*sizeof(float));
static float* vector_field_y_prev = (float*) malloc(SIZE*sizeof(float));
static float* scalar_field_1_prev = (float*) malloc(SIZE*sizeof(float));
static float* scalar_field_2_prev = (float*) malloc(SIZE*sizeof(float));
static float* Dscalar_field_1;
static int* Dt;

inline int DimensionConvert(int i, int j) {
	return i * (W+2) + j;
}

inline int DimensionConvertLittle(int i, int j) {
	return i * W + j;
}

#define SWAP(x, y) {float* temp = x; x = y; y = temp;}

struct Lab1VideoGenerator::Impl {
	int t = 0;
};

Lab1VideoGenerator::Lab1VideoGenerator(): impl(new Impl) {
	memset(vector_field_x, 0, SIZE*sizeof(float));
	memset(vector_field_y, 0, SIZE*sizeof(float));
	memset(scalar_field_1, 0, SIZE*sizeof(float));
	memset(scalar_field_2, 0, SIZE*sizeof(float));
	memset(vector_field_x_prev, 0, SIZE*sizeof(float));
	memset(vector_field_y_prev, 0, SIZE*sizeof(float));
	memset(scalar_field_1_prev, 0, SIZE*sizeof(float));
	memset(scalar_field_2_prev, 0, SIZE*sizeof(float));
	cudaMalloc((void**) &Dscalar_field_1, SIZE*sizeof(float));
	cudaMalloc((void**) &Dt, sizeof(int));
}

Lab1VideoGenerator::~Lab1VideoGenerator() {
	free(vector_field_x);
	free(vector_field_y);
	free(scalar_field_1);
	free(scalar_field_2);
	free(vector_field_x_prev);
	free(vector_field_y_prev);
	free(scalar_field_1_prev);
	free(scalar_field_2_prev);
	cudaFree(Dscalar_field_1);
	cudaFree(Dt);
}

void AddField(float* s1p, float* s2p, float* vxp, float* vyp) {
	memset(s1p, 0, SIZE*sizeof(float));
	memset(s2p, 0, SIZE*sizeof(float));
	memset(vxp, 0, SIZE*sizeof(float));
	memset(vyp, 0, SIZE*sizeof(float));
	s1p[DimensionConvert(400, 320)] = 350.0;
	//s1p[DimensionConvert(430, 360)] = 350.0;
	s2p[DimensionConvert(20, 20)] = 0.0;
	vxp[DimensionConvert(410, 320)] = -15.0;
	vyp[DimensionConvert(320, 470)] = 0.0;
}

void AddForce(float* dst, float* src) {
	for (int i=0 ;i < SIZE; i++) dst[i] += DT*src[i];
}

void SetBoundary(int b, float* dst) {
	//line
	for (int i = 1; i <= H; i++) {
		dst[DimensionConvert(i, 0)] = b == 2 ? -dst[DimensionConvert(i, 1)] : dst[DimensionConvert(i, 1)];
		dst[DimensionConvert(i, W + 1)] = b == 2 ? -dst[DimensionConvert(i, W)] : dst[DimensionConvert(i, W)];
	}
	for (int j = 1; j <= W; j++) {
		dst[DimensionConvert(0, j)] = b == 1 ? -dst[DimensionConvert(1, j)] : dst[DimensionConvert(1, j)];
		dst[DimensionConvert(H + 1, j)] = b == 1 ? -dst[DimensionConvert(H, j)] : dst[DimensionConvert(H, j)];
	}
	//vertex
	dst[DimensionConvert(0, 0)] = 0.5 * (dst[DimensionConvert(1, 0)] + dst[DimensionConvert(0, 1)]);
	dst[DimensionConvert(H + 1, 0)] = 0.5 * (dst[DimensionConvert(H, 0)] + dst[DimensionConvert(H + 1, 1)]);
	dst[DimensionConvert(0, W + 1)] = 0.5 * (dst[DimensionConvert(0, W)] + dst[DimensionConvert(1, W + 1)]);
	dst[DimensionConvert(H + 1, W + 1)] = 0.5 * (dst[DimensionConvert(H + 1, W)] + dst[DimensionConvert(H, W + 1)]);
}

void LinearSolver(int b, float* dst, float* src, float a, float c) {
	for (int count = 0; count < LINEARSOLVERTIMES; count++) {
		for (int i = 1; i <= H; i++) {
			for (int j = 1; j <= W; j++) {
				dst[DimensionConvert(i, j)] = (src[DimensionConvert(i, j)] + a * (dst[DimensionConvert(i - 1, j)] +
																				  dst[DimensionConvert(i + 1, j)] +
																				  dst[DimensionConvert(i, j - 1)] +
																				  dst[DimensionConvert(i, j + 1)])) / c;
			}
		}
		SetBoundary(b, dst);
	}
}

void Diffuse(int b, float* dst, float* src, float diff) {
	int mymax = max(W, H);
	float a = DT * diff * mymax * mymax * mymax;
	LinearSolver(b, dst, src, a, 1+6*a);
}

void Project(float* vx, float* vy, float* p, float* div) {
	for (int i = 1; i <= H; i++) {
		for (int j = 1; j <= W; j++) {
			div[DimensionConvert(i, j)] = -0.5 * ((vx[DimensionConvert(i + 1, j)] - vx[DimensionConvert(i - 1, j)])/H +
												  (vy[DimensionConvert(i, j + 1)] - vy[DimensionConvert(i ,j - 1)])/H);
			p[DimensionConvert(i, j)] = 0;
		}
	}
	SetBoundary(0, div);
	SetBoundary(0, p);
	LinearSolver(0, p, div, 1, 6);
	for (int i = 1; i <= H; i++) {
		for (int j = 1; j <=W; j++) {
			vx[DimensionConvert(i, j)] -= 0.5 * H * (p[DimensionConvert(i + 1, j)] - p[DimensionConvert(i - 1, j)]);
			vy[DimensionConvert(i, j)] -= 0.5 * H * (p[DimensionConvert(i, j + 1)] - p[DimensionConvert(i, j - 1)]);
		}
	}
	SetBoundary(1, vx);
	SetBoundary(2, vy);
}

void Advect(int b, float* dst, float* src, float* vx, float* vy) {
	float dtx, dty, x, y, s0, s1, t0, t1;
	int i0, i1, j0, j1;
	dtx = dty = DT * max(W, H);
	for (int i = 1; i <= H; i++) {
		for (int j = 1; j <=W; j++) {
			x = i - dtx * vx[DimensionConvert(i, j)];
			y = j - dty * vy[DimensionConvert(i, j)];
			if (x<0.5f) x=0.5f; if (x>H+0.5f) x=H+0.5f; i0=(int)x; i1=i0+1;
			if (y<0.5f) y=0.5f; if (y>W+0.5f) y=W+0.5f; j0=(int)y; j1=j0+1;
			s1 = x-i0; s0 = 1-s1; t1 = y-j0; t0 = 1-t1;
			dst[DimensionConvert(i, j)] = s0 * t0 * src[DimensionConvert(i0, j0)]+ 
										  s1 * t0 * src[DimensionConvert(i1, j0)]+
										  s0 * t1 * src[DimensionConvert(i0, j1)]+
										  s1 * t1 * src[DimensionConvert(i1, j1)];
		}
	}
	SetBoundary(b, dst);
}

void VectorFieldUpdate() {
	AddForce(vector_field_x, vector_field_x_prev);
	AddForce(vector_field_y, vector_field_y_prev);
	SWAP(vector_field_x, vector_field_x_prev);
	SWAP(vector_field_y, vector_field_y_prev);
	Diffuse(1, vector_field_x, vector_field_x_prev, VISCOSITY);
	Diffuse(2, vector_field_y, vector_field_y_prev, VISCOSITY);
	Project(vector_field_x, vector_field_y, vector_field_x_prev, vector_field_y_prev);
	SWAP(vector_field_x, vector_field_x_prev);
	SWAP(vector_field_y, vector_field_y_prev);
	Advect(1, vector_field_x, vector_field_x_prev, vector_field_x_prev, vector_field_y_prev);
	Advect(2, vector_field_y, vector_field_y_prev, vector_field_x_prev, vector_field_y_prev);
	Project(vector_field_x, vector_field_y, vector_field_x_prev, vector_field_y_prev);
}

void ScalarFieldUpdate() {
	AddForce(scalar_field_1, scalar_field_1_prev);
	AddForce(scalar_field_2, scalar_field_2_prev);
	SWAP(scalar_field_1, scalar_field_1_prev);
	SWAP(scalar_field_2, scalar_field_2_prev);
	Diffuse(0, scalar_field_1, scalar_field_1_prev, DIFFUSE);
	Diffuse(0, scalar_field_2, scalar_field_2_prev, DIFFUSE);
	SWAP(scalar_field_1, scalar_field_1_prev);
	SWAP(scalar_field_2, scalar_field_2_prev);
	Advect(0, scalar_field_1, scalar_field_1_prev, vector_field_x, vector_field_y);
	Advect(0, scalar_field_2, scalar_field_2_prev, vector_field_x, vector_field_y);
}

void EachLoop() {
	AddField(scalar_field_1_prev, scalar_field_2_prev, vector_field_x_prev, vector_field_y_prev);
	VectorFieldUpdate();
	ScalarFieldUpdate();
}

void Lab1VideoGenerator::get_info(Lab1VideoInfo &info) {
	info.w = W;
	info.h = H;
	info.n_frame = NFRAME;
	// fps = 24/1 = 24
	info.fps_n = 24;
	info.fps_d = 1;
};

__global__ void Threshold(float* src, uint8_t* dst, int* time) {
	int H = 480;
	int W = 640;
	int b=blockIdx.x;       
    int t=threadIdx.x;      
    int n=blockDim.x;       
    int x=b*n+t;
    int row_num = x / (W+2);
    int col_num = x % (W+2);         
    if (row_num == 0 || row_num == (H+1) || col_num == 0 || col_num == (W+1)) return; //handle boundary

    int dst_idx = (row_num -1)*W + col_num-1;
    uint8_t temp = (128*(NFRAME-*time) + 84**time)/NFRAME;
    uint8_t temp1 = (128*(NFRAME-*time) + 255**time)/NFRAME;
    dst[W*H + dst_idx] = temp1;
    dst[5/4*W*H + dst_idx] = temp;
	if (src[x] > 3) {
		dst[dst_idx] = 255;
	} else if (src[x] > 1.5) {
		dst[dst_idx] = 200;
	} else if (src[x] > 0.5) {
		dst[dst_idx] = 110;
	} else if (src[x] > 0.3) {
		dst[dst_idx] = 40;
	} else {
		dst[dst_idx] = 0;
	}
}

void Lab1VideoGenerator::Generate(uint8_t *yuv) {
	//yuv format is row by row
	std::cerr << impl->t << std::endl;
	EachLoop();
	//*************cpu version**************//
	/*uint8_t* s1_y_ratio = new uint8_t[W*H];
	uint8_t temp = (128*(NFRAME-impl->t) + 84*(impl->t))/NFRAME;
	uint8_t temp1 = (128*(NFRAME-impl->t) + 255*(impl->t))/NFRAME;
	cudaMemset(yuv, 0, W*H);
	cudaMemset(yuv + W*H, temp, W*H/4);
	cudaMemset(yuv + 5/4*W*H, temp1, W*H/4);
	for (int i = 0; i < H; i++) {
		for (int j = 0; j < W; j++) {
			if (scalar_field_1[DimensionConvert(i + 1, j + 1)] > 3) {
				s1_y_ratio[DimensionConvertLittle(i, j)] = 255;
			} else if (scalar_field_1[DimensionConvert(i + 1, j + 1)] > 1.5) {
				s1_y_ratio[DimensionConvertLittle(i, j)] = 200;
			} else if (scalar_field_1[DimensionConvert(i + 1, j + 1)] > 0.5) {
				s1_y_ratio[DimensionConvertLittle(i, j)] = 110;
			} else if (scalar_field_1[DimensionConvert(i + 1, j + 1)] > 0.3) {
				s1_y_ratio[DimensionConvertLittle(i, j)] = 40;
			} else {
				s1_y_ratio[DimensionConvertLittle(i, j)] = 0;
			}
		}
	}*/
	//cudaMemcpy(yuv, s1_y_ratio, W*H*sizeof(uint8_t), cudaMemcpyHostToDevice);
	//delete s1_y_ratio;
	
	//***********gpu version*******//
	cudaMemcpy(Dscalar_field_1, scalar_field_1, SIZE*sizeof(float), cudaMemcpyHostToDevice);
	cudaMemcpy(Dt, &(impl->t), sizeof(int), cudaMemcpyHostToDevice);
	//uint8_t* s1_device = new uint8_t[W*H];
	Threshold<<<H+2,W+2>>>(Dscalar_field_1, yuv, Dt);
	cudaThreadSynchronize();
	//cudaMemcpy(s1_device, yuv, W*H*sizeof(uint8_t), cudaMemcpyDeviceToHost);




	//***********debug use********//
	/*cudaMemset(yuv, 76, W*H);
	cudaMemset(yuv+W*H, 84, W*H/4);
	cudaMemset(yuv+5/4*W*H, 255, W*H/4);*/
	/*for (int i = 0; i < H; i++) {
		for (int j = 0; j < W; j++) {
			if (s1_device[DimensionConvertLittle(i, j)] != s1_y_ratio[DimensionConvertLittle(i, j)]) std::cerr<< "("<<i<<","<<j<<")";
		}
	}*/
	++(impl->t);
	//delete s1_y_ratio;

}
