#include "counting.h"
#include <cstdio>
#include <cassert>
#include <thrust/scan.h>
#include <thrust/transform.h>
#include <thrust/functional.h>
#include <thrust/device_ptr.h>
#include <thrust/execution_policy.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <iostream>

#define BLOCKDIM 512
#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, const char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}

__device__ __host__ int CeilDiv(int a, int b) { return (a-1)/b + 1; }
__device__ __host__ int CeilAlign(int a, int b) { return CeilDiv(a, b) * b; }

__global__ void ConvertToZeroOne(const char *text, int* pos) {
	int index = blockIdx.x*blockDim.x + threadIdx.x;
	if (text[index] == '\n') {
		pos[index] = 0;
	} else {
		pos[index] = 1;
	}
	__syncthreads();
	int count = 0;
	for (int k = 0; k <500 && index - k >=0; k++) {
		if (pos[index - k] == 0) break;
		count += 1;
	}
	__syncthreads();
	pos[index] = count;
}

__global__ void ConvertToZeroOne2(const char *text, int* pos, int* temp) {
	int index = blockIdx.x*blockDim.x + threadIdx.x;
	if (text[index] == '\n') {
		temp[index] = 0;
	} else {
		temp[index] = 1;
	}
	__syncthreads();
	int count = 0;
	for (int k = 0; k <500 && index - k >=0; k++) {
		if (temp[index - k] == 0) break;
		count += 1;
	}
	pos[index] = count;
}
__global__ void ConvertToZeroOne3(const char *text, int* pos, int* temp) {
	int index = blockIdx.x*blockDim.x + threadIdx.x;
	if (text[index] == '\n') {
		temp[index] = 0;
	} else {
		temp[index] = 1;
	}
	__syncthreads();
	int count = 0;
	int k = 0;
	int range_index = index;
	while (k < 500) {
		if (range_index < 0) break;
		if (temp[range_index] == 0) break;
		count++;
		k++;
		range_index--;
	}
	pos[index] = count;
}


struct text_functor {
	__host__ __device__ int operator()(const char t) const {
		if (t == '\n') return 0;
		return 1;
	}
};

void CountPosition1(const char *text, int *pos, int text_size)
{
	thrust::device_ptr<const char> t(text);
	thrust::device_ptr<int> p(pos);
	thrust::transform(t, t + text_size, p, text_functor());
	//p[0] = 1;
	thrust::inclusive_scan_by_key(thrust::device, p, p + text_size, p, p);
}

void CountPosition2(const char *text, int *pos, int text_size)
{
	//ver1 ~380,000us
	/*
	int grid_dim = CeilDiv(text_size, BLOCKDIM);
	ConvertToZeroOne<<< grid_dim, BLOCKDIM>>>(text, pos);
	*/

	//ver2 ~300,000us
	int grid_dim = CeilDiv(text_size, BLOCKDIM);
	int* temp;
	cudaMalloc((void**) &temp, sizeof(int)*text_size);
	cudaMemset(temp, 0, sizeof(int)*text_size);
	ConvertToZeroOne2<<< grid_dim, BLOCKDIM>>>(text, pos, temp);

	//ver3 ~330,000us
	/*
	int grid_dim = CeilDiv(text_size, BLOCKDIM);
	int* temp;
	cudaMalloc((void**) &temp, sizeof(int)*text_size);
	cudaMemset(temp, 0, sizeof(int)*text_size);
	ConvertToZeroOne3<<< grid_dim, BLOCKDIM>>>(text, pos, temp);
	*/
}
